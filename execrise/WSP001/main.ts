import readlineSync from 'readline-sync'

readlineSync.question("今日心情係點呢?")

let student = "Alex" // type inference

let student2:string = "Gordon"

let age:number|string = 10 // <<<
age = "Ten"

let graduated:boolean = false

///////////////

//array
let names:string[] = ["alex","gordon","michael"]


//Object
let studentInfo:{
    name:string,
    age:number,
    height:number,
    interests:string[],
} = {
    "name":"Alex",
    age:10,
    height: 130 ,
    interests: ["Badminaton", "Running"]
}
studentInfo

//function

function helloWorld(name:string){
    console.log("hello world," + name)
}

const helloWorldVar: () => string = () => {
    console.log("Hello World")
    return "good"
}

const helloWorldVar2: () => string = function(){
    console.log("Hello World")
    return "ok..."
}

//example that must need type

let foods: string[] = [];
foods.push("abc")

let obj: {
    something?: string
} = {}

obj.something?.toUpperCase() //無野就無事發生
//obj.something!.toUpperCase() //死都要行
obj.something = "tecky"


console.log(obj.something)
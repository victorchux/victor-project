let lifes = [
]

function setup() {
  createCanvas(1000, 500)

  for (let i = 0; i < 20; i++) {
    let lifeRows = []
    for (let j = 0; j < 20; j++) {
      lifeRows.push(Math.round(Math.random()))
    }
    lifes.push(lifeRows)
  }

  console.log(lifes)
}

function draw() {
  background('#fff')

  for (let i = 0; i < 20; i++) {
    for (let j = 0; j < 20; j++) {
      if (lifes[i][j]) {
        fill('#00ff99')
      } else {
        fill('#ff9900')
      }
      rect(20 * j, 20 * i, 20, 20)
    }
  }
  rect(mouseX, mouseY, 20, 20)

  // calculate 周圍生命數量
  let lifeCounters = []
  for (let i = 0; i < 20; i++) {
    lifeCounters[i] = [];
    for (let j = 0; j < 20; j++) {
      lifeCounters[i][j] = 0;
      if (i > 0 && lifes[i - 1][j - 1]) {
        lifeCounters[i][j]++
      }
      if (i > 0 && lifes[i - 1][j]) {
        lifeCounters[i][j]++
      }
      if (i > 0 && lifes[i - 1][j + 1]) {
        lifeCounters[i][j]++
      }
      if (lifes[i][j - 1]) {
        lifeCounters[i][j]++
      }
      if (lifes[i][j + 1]) {
        lifeCounters[i][j]++
      }
      if (i < 20 - 1 && lifes[i + 1][j - 1]) {
        lifeCounters[i][j]++
      }
      if (i < 20 - 1 && lifes[i + 1][j]) {
        lifeCounters[i][j]++
      }
      if (i < 20 - 1 && lifes[i + 1][j + 1]) {
        lifeCounters[i][j]++
      }
    }
  }

  // 決定生死
  for (let i = 0; i < 20; i++) {
    for (let j = 0; j < 20; j++) {
      if (lifes[i][j]) {
        if (lifeCounters[i][j] < 2 || lifeCounters[i][j] > 3) {
          lifes[i][j] = 0
        }
      } else {
        if (lifeCounters[i][j] == 3) {
          lifes[i][j] = 1
        }
      }
    }
  }
}

function mouseDragged() {
  const i = Math.floor(mouseY / 20)
  const j = Math.floor(mouseX / 20)

  lifes[i][j] = 1
  fill('#00ff99')
  rect(20 * j, 20 * i, 20, 20)
}
function mousePressed() {
  noLoop();
}
function mouseReleased() {
  loop();
}





class pattern {
    //private pattern: string

    constructor(pattern){
        this.pattern = pattern;
    }

    draw(life,x,y){
        const lines = this.pattern.split("n")
        for (let i = 0; i < lines.length; i++){
            
        }
    }
}
function compareCard(cardA,cardB){
    const ranks = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"];
    const suits = ["Diamond","Club","Heart","Spade"];
    const [suitA,rankA] = cardA;
    const [suitB,rankB] = cardB;
    const ranksDiff = ranks.indexOf(rankA) - ranks.indexOf(rankB);
    if(ranksDiff !== 0){
      return ranksDiff;
    }else{
      return suits.indexOf(suitA) - suits.indexOf(suitB);
    }
}



const cards = [
  ['Spade','A'],
  ['Diamond','J'],
  ['Club','3'],
  ['Heart','6'],
  ['Spade','K'],
  ['Club','2'],
  ['Heart','Q'],
  ['Spade','6'],
  ['Heart','J'],
  ['Spade','10'],
  ['Club','4'],
  ['Diamond','Q'],
  ['Diamond','3'],
  ['Heart','4'],
  ['Club','7']
];
//Q1
console.log(cards.reduce(function(count,current){
  if (current[0] == 'Spade'){
    return count + 1;
  }else {
    return count + 0;
  }
},0));

//Q2
console.log(cards.filter(function(elem){
  return elem[1] < 3 || elem[0] == 'Diamond' && elem[1] == 3;
}))


//Q3
console.log(cards.reduce(function(count, current){
    if ((current[0] == 'Diamond'|| current[0] == 'Heart') && (current[1] == "J" || current[1] == "Q"||current[1] == "K"||current[1] == "A")){
  return count + 1;
} else{
  return count + 0;
  }
}, 0 ))


//Q4
console.log(cards.map(function(card){
  return [card[0].replace('Club','Diamond') , card[1]]
}))


//Q5
console.log(cards.map(function(card){
  return [card[0]  , card[1].replace('A' , 2) ]
}))
import fs from "fs";


function listAllJsRecursive(path: string): void {

    fs.readdir(path, {withFileTypes: true}, function (err: Error, files: any){

        for (let i = 0; i < files.length; i++){
            if (files[i].isDirectory()){
                listAllJsRecursive(path + files[i].name+ '/')
            }else{
                
                const regexpLastWord = RegExp(/\.js$/);
                if (regexpLastWord.test(files[i].name)){
                    console.log(path + files[i].name)
                } 
            }
        }
    })};
        
listAllJsRecursive('/mnt/d/Tecky/execrise/WSP003/');
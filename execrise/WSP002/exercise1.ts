
// Declaration of Class and its methods
class Player {
    private strength: number;
    private name: string;
    private experience: number;
    constructor(strength: number, name: string) {
        this.strength = strength;
        this.name = name;
    }


    public attack(monster: Monster) {
        while (monster.getHP() > 0) {
            if (Math.random() > 0.5) {
                monster.injure(this.strength * 2)
                console.log(`Player ${this.name} attacks ${monster.name} (HP: ${monster.getHP()}) [CRITICAL]`)
            } else {
                monster.injure(this.strength)
                console.log(`Player ${this.name} attacks ${monster.name} (HP: ${monster.getHP()})`)
            }

        }
    }



    public gainExperience(exp: number) {
        this.experience = + this.strength;
        console.log(`Player ${this.name} gains ${this.strength} exp ! (experience: ${this.experience}) `);
    }

}


export class Monster {
    private HP: number;  // Think of how to write injure
    public name: string;

    constructor(HP: number, name: string) {
        this.HP = HP
        this.name = name
    }

    public injure(strength: number) {
        monster.HP = monster.HP - strength
    }

    public getHP() {
        return this.HP;
    }
}


// Invocations of the class and its methods
const player = new Player(20, 'Peter');
const monster = new Monster(100, 'devil');


player.attack(monster);

// English counterpart: Player attacks monster


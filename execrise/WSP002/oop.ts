// 異性
// 有錢
// 跳舞

interface GirlFriend extends Walkable {
  isRich(): boolean;
  dance(): void;
  getGender(): string;
}

// class GirlFriend {
//   private gender:string = "female"
//   public isRich() {
//     return true;
//   }
//   public dance() {
//     console.log('💃🏻')
//   }
//   public getGender() {
//     return this.gender;
//   }
// }

interface Walkable {
  walk(): void; 
}

interface CanAttendLesson {
  attendLesson(): void;
}

abstract class Animal {
  public isAlive() {}
}

// access modifier: public, private,   protected
//                  公廁     主人房套廁  屋企廁

class Student extends Animal implements Walkable, CanAttendLesson {
  // properties 
  private iq: number = 90; // Encapsulation

  constructor(iq: number) {
    super();
    this.iq = iq
  }

  // methods
  public graduate() {
    console.log("完鳥！！！！正")
  }

  public attendLesson() {
    this.gainKnowledge()
  }

  public eat() {
    this.gainKnowledge()
  }

  public getIQ() {
    return this.iq;
  }

  public walk() {
    console.log("😑")
  }

  protected gainKnowledge() {
    this.iq += 10
  }
}

// Inheritance
class CleverStudent extends Student {
  // Method override
  public attendLesson() {
    this.gainKnowledge()
    this.gainKnowledge()
  }
}

class Cat extends Animal implements Walkable, CanAttendLesson, GirlFriend {
  isRich(): boolean {
    return true
  }
  dance(): void {
    console.log('🐈')
  }
  getGender(): string {
    return "female";
  }
  public attendLesson() {
    
  }
  public walk() {
    console.log("Meow~")
  }
}

// const alex = new Animal();
const adam = new Student(100)
const eve = new CleverStudent(110)
const milktea = new Cat()

const cohort13: Student[] = [adam, eve]
const cohort13walkable: Walkable[] = [adam, eve, milktea]

// Polymorphism
for (let student of cohort13) {
  student.attendLesson()
}

for (let walkable of cohort13walkable) {
  walkable.walk()
}

console.log("adam IQ: " + adam.getIQ())
console.log("eve IQ: " + eve.getIQ())
console.log(milktea.getGender())

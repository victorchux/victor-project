interface Attack{
    damage:number;
    name:string;
    getDamage:()=>number;
 }
 
 class BowAndArrow implements Attack{
    damage:number;
    name = "BowAndArrow"
    constructor(attack:number){
        this.damage = attack
    }
    public getDamage(){
        return this.damage;
    }
    
        //Bow and Arrow Attack here
 }
 
 class ThrowingSpear implements Attack{
    damage:number;
    name = "ThrowingSpear";
    constructor(attack:number){
        this.damage = attack
    }// Throwing Spear Attack here
    public getDamage(){
        return this.damage;
    }
 }
 
 
 interface Player{
     attack(monster:Monster): void;
     switchAttack(): void;
     gainExperience(exp:number): void;
 }
 
 class Amazon implements Player{
     private primary: Attack;
     private secondary: Attack;
     private usePrimaryAttack: boolean;
     private name: string;
     private EXP:number = 0;

     constructor(name:string){
         this.primary = new BowAndArrow(30)//(/*Need some params here*/);
         this.secondary = new ThrowingSpear(40)
         this.name = name;
         //(/*Need some params here*/);
         // TODO: set the default value of usePrimaryAttack
      }
 
      attack(monster:Monster):void{
          if(this.usePrimaryAttack){
             console.log(`${monster.name} got ${ this.primary.getDamage()} damage.`)// TODO: use primary attack
          }else{
             console.log(`${monster.name} got ${this.secondary.getDamage()} damage.`)// TODO: use secondary attack
          }
      }
 
      switchAttack(){
          if(this.usePrimaryAttack == true){
              console.log(`Changed to ${this.secondary.name} mode`)
              return this.usePrimaryAttack = false;
          }else{
            console.log(`Changed to ${this.primary.name} mode`)
              return this.usePrimaryAttack = true;
              
          }// TODO: Change the attack mode for this player
      };
      

      gainExperience(){
        if(this.usePrimaryAttack == true){
            this.EXP += 5;
            console.log( ` ${this.name} +5 EXP (EXP: ${this.EXP})!!`)
        }else{
            this.EXP += 10;
            console.log( ` ${this.name} +10 EXP (EXP: ${this.EXP})!!`)
        }            
      }
      //.. The remaining methods.
 }
 
 class Monster{
    public HP:number;  
    public name:string;
    constructor(HP:number, name:string){
        this.HP = HP
        this.name = name
 }
}

const monster:Monster = new Monster(100,'devil');
const player:Amazon = new Amazon("Peter");

player.attack(monster);
player.gainExperience();
player.switchAttack();
player.attack(monster);
player.gainExperience();
player.switchAttack();
player.attack(monster);
player.gainExperience();
player.switchAttack();
player.attack(monster);
player.gainExperience();
player.attack(monster);
player.gainExperience();
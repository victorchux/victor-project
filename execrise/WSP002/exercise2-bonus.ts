import { Monster } from './exercise1'

interface Attack{
  damage:number
}

class BowAndArrow implements Attack{
  damage:number

  public constructor(damage: number) {
    this.damage = damage;
  }
}

class ThrowingSpear implements Attack{
  damage:number

  public constructor(damage: number) {
    this.damage = damage;
  }
}

class MagicSpell implements Attack{
  damage:number

  public constructor(damage: number) {
    this.damage = damage;
  }
}

class Sword implements Attack{
  damage:number

  public constructor(damage: number) {
    this.damage = damage;
  }
}

interface Player{
   attack(monster:Monster): void;
   switchAttack(): void;
   gainExperience(exp:number): void;
}

class BasePlayer implements Player{
   private secondary: Attack;
   private usePrimaryAttack: boolean;

   // Composition
   // Inversion of Control
   constructor(private primary: Attack, secondary: Attack){
      this.secondary = secondary;
      this.usePrimaryAttack = true;
    }
      
    gainExperience(exp: any) {
      throw new Error("Method not implemented.");
    }

    attack(monster:Monster):void{
        if(this.usePrimaryAttack){
          monster.injure(this.primary.damage)
        }else{
          monster.injure(this.secondary.damage)
        }
    }

    switchAttack(){
      this.usePrimaryAttack = !this.usePrimaryAttack;
    }

    //.. The remaining methods.
}

class Amazon extends BasePlayer {
  constructor() {
    super(new BowAndArrow(30), new ThrowingSpear(40));
  }
}

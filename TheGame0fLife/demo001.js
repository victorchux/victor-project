const unitLength = 20;
const strokeColor = 220;
let columns;
let rows;
let currentBoard;
let nextBoard;
let silderFrame;
let SurvivalboxColor;
let DeathboxColor;
let loneliness = document.querySelector("#loneliness");
let overpopulation = document.querySelector("#overpopulation");
let reproduction = document.querySelector("#reproduction");


function setup() {
    const canvas = createCanvas(1000, 500);
    canvas.parent(document.querySelector('.canvas'));
    sliderFrame = createSlider(1, 50, 5);
    sliderFrame.parent(document.querySelector('.sliderFrame'));
    SurvivalboxColor = createSlider(1, 340, 1);
    SurvivalboxColor.parent(document.querySelector('.SurvivalColor'));
    DeathboxColor = createSlider(1, 340, 1);
    DeathboxColor.parent(document.querySelector('.DeathColor'));

    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }


    init();

    function init() {
        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                currentBoard[i][j] = 0;
                nextBoard[i][j] = 0;
            }
        }
    }

    document.querySelector('#reset')
        .addEventListener('click', function () {
            init();

        });

    function rand() {
        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                currentBoard[i][j] = random() > 0.8 ? 1 : 0;
                nextBoard[i][j] = 0;
            }
        }
    };

    rand();

    document.querySelector("#rand")
        .addEventListener('click', function () {
            rand();
        });
}


function draw() {
    frameRate(sliderFrame.value());
    colorMode(HSL);
    background(255, 10, 50);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == nextBoard[i][j] && currentBoard[i][j] == 1) {
                fill(DeathboxColor.value(), 50, 50);
            }
            else if (currentBoard[i][j] == 1) {
                fill(SurvivalboxColor.value(), 85, 57);
            } else {
                fill(192, 15, 97);
            }
            strokeWeight(2);
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
    function generate() {

        for (let x = 0; x < columns; x++) {
            for (let y = 0; y < rows; y++) {

                let neighbors = 0;
                for (let i of [-1, 0, 1]) {
                    for (let j of [-1, 0, 1]) {
                        if (i === 0 && j === 0) {
                            continue;
                        }
                        neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                    }
                }

                if (currentBoard[x][y] == 1 && neighbors < loneliness.value) {
                    nextBoard[x][y] = 0;
                } else if (currentBoard[x][y] == 1 && neighbors > overpopulation.value) {
                    nextBoard[x][y] = 0;
                } else if (currentBoard[x][y] == 0 && neighbors == reproduction.value) {
                    nextBoard[x][y] = 1;
                } else {
                    nextBoard[x][y] = currentBoard[x][y];
                }
            }
        }

        [currentBoard, nextBoard] = [nextBoard, currentBoard];
    }
}

function mouseDragged() {

    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(192, 3, 71);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}





function mousePressed() {
    if (mouseX > 450) {
        noLoop();
        mouseDragged();
    }
}

function mouseReleased() {
    if (mouseX > 450) { }
    loop();
};

let timer = true;
document.querySelector("#run").addEventListener('click', function () {
    if (timer == true) {
        noLoop();
        timer = false;
        document.querySelector("#run").innerHTML = ("Start")
    } else {
        loop();
        timer = true;
        document.querySelector("#run").innerHTML = ("Stop")
    }
});

let timer2 = true;
document.querySelector("#step").addEventListener('click', function(){
    if (timer2 == true) {
        draw();
        noLoop();
        timer2 == false;
    } 
})
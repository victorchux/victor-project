let lifes = []

function setup() {
    createCanvas(1000, 500);
    for (let i = 0; i < 20; i++) {
        let lifeRows = [];
        for (let j = 0; j < 20; j++) {
            lifeRows.push(Math.round(Math.random()));
        }
        lifes.push(lifeRows);
    }
    console.log(lifes);
}


function draw() {
    background(220);
    for (let i = 0; i < 20; i++) {
        for (let j = 0; j < 20; j++) {
            if(lifes[i][j]){
                fill('#159391');
            }else{
                fill('#00b7cf');
            }
            rect(20 * j, 20 * i, 20, 20);
        }
    }

    let lifecounters = []
    for( let i = 0; i<20; i++){
        lifecounters[i]=[];
        for (let j = 0; j < 20; j++){
            lifecounters[i][j] = 0;
            if( i > 0 && lifes[i-1][j-1]){
                lifecounters[i][j]++;
            }
            if ( i  > 0 && lifes[i-1][j]){
                lifecounters[i][j]++;
            }
            if( i > 0 && lifes[i-1][j+1]){
                lifecounters[i][j]++;
            }
            if(lifes[i][j-1]){
                lifecounters[i][j]++;
            }
            if(lifes[i][j+1]){
                lifecounters[i][j]++;
            }
            if( i <20-1 && lifes[i+1][j-1]){
                lifecounters[i][j]++;
            }
            if( i <20-1 && lifes[i+1][j]){
                lifecounters[i][j]++;
            }
            if( i <20-1 && lifes[i+1][j+1]){
                lifecounters[i][j]++;
            }
        }
    }
    console.log(lifecounters)

    for(let i = 0; i < 20; i++){
        for (let j=0; j < 20; j++){
            if (lifecounters[i][j] < 2 || lifecounters[i][j] > 3){
                lifes[i][j] = 0;
            }
            if(lifecounters[i][j] ==3){
                lifes[i][j] = 1;
            }
        }
    }
}
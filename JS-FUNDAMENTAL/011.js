let x = 0 
let arr = ['alex', 'gordon', 'micheal']

//condition
while (x <arr.length){
    console.log(arr[x])
    x = x+1;
}

for (let i = 0; i < arr.length; i = i+1){
    console.log(arr[i])
}

//for-of loop 針對array
for(const name of arr){
    console.log(name)
}

//for-in loop 針對object
const count = {
    a: 2,
    b: 1,
    c: 2,
    d: 2,
    e: 1
}

for (const key in count){
    console.log(key + ' has be shown for ' + count[key]+ ' times.')
}

let numbers = `5 30 31 43 48
2 11 13 45 46 49
11 14 21 28 37 44
18 29 32 33 36 40
2 20 24 30 32 46
5 17 35 37 42 49
1 24 25 27 31 37
15 17 29 30 34 37
5 10 18 20 28 33
1 22 25 27 31 36`.split("\n").join(" ").split(" ")

console.log(numbers)

const check0ccurence ={}
for (const number of numbers){
    if (check0ccurence[number] == null){
        check0ccurence[number] = 0
    }
    check0ccurence[number] += 1
}

console.log(check0ccurence)

let maxNumber;
let max0ccurence = 0;

for (const number in check0ccurence){
    if (check0ccurence[number]>max0ccurence){
        maxNumber = number;
        max0ccurence = check0ccurence[number];
    }    
}

console.log(`${maxNumber} has shown the most. Its occurence is ${max0ccurence} `)
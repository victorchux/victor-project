//const add = require ('date-fns/add')
//const parseISO = require('date-fns/parseISO')
//console.log(add(new Date(), {days: 100}))

//const readlineSync = require('readline-sync')

//const birthday = readlineSync.question('What is your birthday?')
//console.log(add(parseISO(birthday), {days:10000}))

const readlineSync = require('readline-sync')
const parseISO = require('date-fns/parseISO')
const add = require('date-fns/add')
const format = require('date-fns/format')


const dateString = readlineSync.question('Enter the key date: ')
const date = parseISO(dateString)

console.log('100-day celebration: '+ format(add(date, {days:100}), 'yyyy MM dd'))
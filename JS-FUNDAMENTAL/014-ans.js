const readlineSync = require ('readline-sync');
const fs = require('fs')

const tasks = fs.readFileSync ('014-ansTodoList.txt','utf8').split("\n")

console.log('You have below tasks')
for(let i = 0; i <tasks.length; i++){
    console.log(`[${i+1}] ${tasks[i]}`)
}

const action = readlineSync.question('If you want to add a task, type `+`. If you want to clear a task, type the number (1-' +tasks.length + '):' )



if (action == '+'){
    const newTasks = readlineSync.question('What task is it?' )
    tasks.push(newTasks)
    fs.writeFileSync('014-ansTodoList.txt', tasks.join("\n"))
} else {
    const clearTask = parseInt(action);

    if (!isNaN(clearTask)){
        tasks.splice(clearTask -1 , 1)
        fs.writeFileSync('014-ansTodoList.txt', tasks.join("\n"))
    }
    console.log(tasks);
}
const students = [
    {"name": "Gordon", "age": 30},
    {"name": "Alex", "age": 31},
    {"name": "Michael", "age":32},
]

//Mutation(會對array作出改變)
//仲有array.push, array.pop, array.shift, array.unshift
students.sort(function(studentA, studentB){
    if (studentA.name > studentB.name){
        return +1;
    }else if (studentA.name < studentB.name){
        return -1;
    }
    else{
        return 0;
    }
});

//Immutation(不會對array作出改變)
console.log (students);

console.log(students.findIndex(function(student){
    if (student.name == "Alex"){
        return true;
    } else{
        return false;
    }
}))

console.log(students.find(function(student){
    if (student.name == "Alex"){
        return true;
    }else{
        return false;
    }
}))

//仲有.map .filter .reduce
console.log(students.map(function(student){
    return student.age
}))


console.log(students.filter(function(student){
   if (student.age <=30){
       return true;
   } else {
       return false;
   }   
}))

//let x = 0;
//let qualifiedStudents= [];
//while ( x <students.length){
//    if (students[x].age <= 30){
//        qualifiedStudents.push(students[x])
//    }
//    x++;
//}
//console.log(qualifiedStudents);


//filter + map

console.log(students.filter(function(student){
    return (student.age > 30)
}).map(function(student){
    return student.name
}))

// .reduce
console.log(students.reduce(function(pervious, current) {
    return pervious + current.age;
}, 0))

console.log(students.reduce(function(previous, current){
    if (current.age < previous){
        return current.age
    }else {
        return previous;
    }
}, 9999))

console.log(students.reduce(function(previous, current){
    if (current.age < previous.age){
        return current;
    }else {
        return previous;
    }
}, {"name": "no one", "age":9999}).name)


// exercise

temperatures = [
    {"date": "1 Jan" , "minTemperature": 11 , "maxTemperature": 18},
    {"date": "2 Jan" , "minTemperature": 10 , "maxTemperature": 17},
    {"date": "3 Jan" , "minTemperature": 12 , "maxTemperature": 17},
    {"date": "4 Jan" , "minTemperature": 14 , "maxTemperature": 19},
    {"date": "5 Jan" , "minTemperature": 16 , "maxTemperature": 21},
    {"date": "6 Jan" , "minTemperature": 17 , "maxTemperature": 21},
    {"date": "7 Jan" , "minTemperature": 18 , "maxTemperature": 22},
]

//clone //.slice()

const sortingTemperatures = temperatures.slice()
//exercise 1 .sort
sortingTemperatures.sort(function(tempA,tempB){
    if (tempA.minTemperature > tempB.minTemperature){
        return +1
    } else if (tempA.minTemperature < tempB.minTemperature){
        return -1
    } return 0;
})
console.log(sortingTemperatures)

//faster solution
sortingTemperatures.sort(function(tempA,tempB){
    return tempA.minTemperature - tempB.minTemperature;
})
console.log(sortingTemperatures)

//exercise 2 .map
console.log(temperatures.map(function(temp){
    return temp.date
}))

//exercise 3 .filter
console.log(temperatures.filter(function(temp){
    if (temp.maxTemperature > 20){
        return true;
    }return false;
}).map(function(temp){
    return temp.date
}))

//exercise 4 .reduce
console.log(temperatures.reduce(function(pervious,current){
    if (current.minTemperature < pervious.minTemperature){
        return current;
    }else {
        return pervious;
    }
},{"date": "no date" , "minTemperature": 9999 , "maxTemperature": 9999}).date)
const pts  = 10.9;
const reb  = 9.4;
const ast = 2.4;
const stl = 0.9 ;
const blk = 1.1;
const missedFG = 7.6 - 4.5; //percentage
const missedFT = 3.2 - 1.5;
const TO = 1.5;
const GP = 58 ;

const efficiency = (pts + reb + ast+stl + blk - missedFG - missedFT - TO)/GP

console.log(efficiency);
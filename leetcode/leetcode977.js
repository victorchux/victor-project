let squareNums = [];

var sortedSquares = function(nums) {
    for(let num of nums){
       squareNums.push(num*num);
    }
    
    return squareNums.sort(function(squareA,squareB){
        return squareA -squareB;
    });
};


console.log(sortedSquares([-7,-3,2,3,11]))
